import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'firstChar'
})
export class FirstCharPipe implements PipeTransform {

  transform(value: string): string {
    const arrayValue = value.split(' ');
    return arrayValue[0][0] + ' ' + arrayValue[1][0];
  }

}
