import { Component, OnInit } from '@angular/core';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';
// Ce composant est appelé avec l'URL /dashboard. Il affichera une liste d'article
// Utilisation de ngFor dans le template HTML pour lister tous nos articles
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  preferedArticle: Article;
  // Liste de nos articles à afficher (sale). On verra plus tard comment faire plus propre
  // Pour chaque element article, nous appelons le constructeur grâce au mot clé new
  // new Article() appelera donc le constructeur de notre class Article
  articles: Article[];
  constructor(private articleService: ArticleService) {
  }

  ngOnInit() {
    this.articles = this.articleService.getArticles();
  }

  refreshArticle() {
    this.articles = this.articleService.getArticles();
  }

  setPreferedArticle($event) {
    this.preferedArticle = $event;
  }

  unsetPreferedArticleFunction($event) {
    this.preferedArticle = null;
  }

  addArticle($event) {
    this.articles.push($event);
  }
}
