import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ArticleService} from '../../services/article.service';
import {Article} from '../../models/article';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-edit-article',
  templateUrl: './edit-article.component.html',
  styleUrls: ['./edit-article.component.css']
})
export class EditArticleComponent implements OnInit {
  articleToEdit: Article;
  typeDispo: string[];
  constructor(private activatedRoute: ActivatedRoute, private articleService: ArticleService,
              private toastr: ToastrService, private router: Router) { }

  ngOnInit(): void {
    let id = +this.activatedRoute.snapshot.paramMap.get('id');
    this.articleToEdit = this.articleService.getArticleById(id);
    this.typeDispo = this.articleService.typeDispo;
  }
  editArticle() {
    this.articleService.updateArticle(this.articleToEdit);
    this.toastr.success('Article mis à jour', 'OK');
    this.router.navigate(['/dashboard']);
  }

}
