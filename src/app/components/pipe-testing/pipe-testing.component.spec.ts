import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PipeTestingComponent } from './pipe-testing.component';

describe('PipeTestingComponent', () => {
  let component: PipeTestingComponent;
  let fixture: ComponentFixture<PipeTestingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PipeTestingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PipeTestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
