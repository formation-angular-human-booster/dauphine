import { Component, OnInit } from '@angular/core';
import {Article} from '../../models/article';

@Component({
  selector: 'app-pipe-testing',
  templateUrl: './pipe-testing.component.html',
  styleUrls: ['./pipe-testing.component.css']
})
export class PipeTestingComponent implements OnInit {
  percentValue = 0.4;
  dateValue = new Date();
  objectArticleValue = new Article(1, 'Super match de l\'ASM', 'sport', new Date(),
    'super', 'assets/img/asm.png');
  firstnameValue = 'Aurélien';
  fullName = 'Aurélien Delorme';
  constructor() { }

  ngOnInit() {
  }

}
