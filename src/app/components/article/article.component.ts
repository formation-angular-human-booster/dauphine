import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.css']
})
export class ArticleComponent implements OnInit {
  @Input() article: Article;
  @Input() preferedArticle: Article;
  @Output() preferedArticleEvent: EventEmitter<Article> = new EventEmitter<Article>();
  @Output() unsetPreferedArticleEvent: EventEmitter<Article> = new EventEmitter<Article>();
  @Output() deletedArticle: EventEmitter<boolean> = new EventEmitter<boolean>();
  typeDispo: string[];

  constructor(private articleService: ArticleService, private toastrService: ToastrService) { }

  ngOnInit() {
    this.typeDispo = this.articleService.typeDispo;
  }

  selectPreferedArticle() {
    this.preferedArticleEvent.emit(this.article);
  }

  unsetPreferedArticle() {
    this.unsetPreferedArticleEvent.emit(this.article);
  }

  deleteArticle() {
    // appel notre service article service pour supprimer un article
    this.articleService.deleteArticle(this.article);
    // notifie l'utilisateur pour lui confirmer la suppression
    this.toastrService.warning('Article supprimé', 'Effectué');
    // Envoie un événement au dashboard pour lui dire qu'il y a eu une maj
    // Le dashboard mettra à jour sa liste d'article.
    this.deletedArticle.emit(true);
  }

}
