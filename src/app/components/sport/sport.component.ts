import { Component, OnInit } from '@angular/core';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';

@Component({
  selector: 'app-sport',
  templateUrl: './sport.component.html',
  styleUrls: ['./sport.component.css']
})
export class SportComponent implements OnInit {
  articles: Article[];
  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    this.articles = this.articleService.getArticlesByCategory('sport');
  }

}
