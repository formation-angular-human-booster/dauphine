import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';

// Composant qui sera appelé avec l'url /article/:id. Il pourra afficher tous nos articles
// en fonction de l'id passer dans l'URL
@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {
  // Variable qui contiendra l'id contenu dans notre URL. Il sera initialisé dans
  // la fonction ngOnInit avec l'utilisation de ActivatedRoute
  idArticle: number;
  // Article que nous afficherons dans notre .component.html. Il sera initialisé dans la
  // fonction ngOnInit
  article: Article;

  // Ici nous avons une injection de Activated route. Cela permet de reccupérer les paramètres
  // que nous passons dans notre URL (dans notre cas : l'id de l'article à afficher)
  constructor(private activatedRoute: ActivatedRoute, private articleService: ArticleService) { }

  // Fonction qui est appelée une fois que notre page est chargée
  ngOnInit() {
    this.idArticle = +this.activatedRoute.snapshot.paramMap.get('id');
    this.article =
      this.articleService.getArticleById(this.idArticle);
  }
}
