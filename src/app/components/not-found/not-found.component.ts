import { Component, OnInit } from '@angular/core';

// Ce composant sera appelé si aucune page n'a été trouvé
// Ceci grâce à la ligne suivante dans notre fichier app-routing.module.ts
@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
