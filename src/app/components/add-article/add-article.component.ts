import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Article} from '../../models/article';
import {ArticleService} from '../../services/article.service';
import {Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-add-article',
  templateUrl: './add-article.component.html',
  styleUrls: ['./add-article.component.css']
})
export class AddArticleComponent implements OnInit {
  articleToAdd = new Article() ;
  typeDispo: string[];
  constructor(private articleService: ArticleService, private router: Router,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.typeDispo = this.articleService.typeDispo;
  }

  addArticle() {
      this.articleService.addArticle(this.articleToAdd);
      this.toastr.success('Article ajouté avec succes',
        'Félicitation');
      this.router.navigate(['/dashboard']);
  }

}
