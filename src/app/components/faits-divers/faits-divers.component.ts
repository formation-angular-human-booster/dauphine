import { Component, OnInit } from '@angular/core';
import {ArticleService} from '../../services/article.service';
import {Article} from '../../models/article';

@Component({
  selector: 'app-faits-divers',
  templateUrl: './faits-divers.component.html',
  styleUrls: ['./faits-divers.component.css']
})
export class FaitsDiversComponent implements OnInit {
  articles: Article[];
  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    this.articles = this.articleService.getArticlesByCategory('faits-divers');
  }

}
