import { Component, OnInit } from '@angular/core';
import {ArticleService} from '../../services/article.service';
import {Article} from '../../models/article';

@Component({
  selector: 'app-sante',
  templateUrl: './sante.component.html',
  styleUrls: ['./sante.component.css']
})
export class SanteComponent implements OnInit {
  articles: Article[];
  constructor(private articleService: ArticleService) { }

  ngOnInit() {
    this.articles = this.articleService.getArticlesByCategory('sante');

  }

}
