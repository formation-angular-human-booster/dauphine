import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MenuComponent } from './components/menu/menu.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ArticleComponent } from './components/article/article.component';
import { SportComponent } from './components/sport/sport.component';
import { SanteComponent } from './components/sante/sante.component';
import { FaitsDiversComponent } from './components/faits-divers/faits-divers.component';
import { ArticleDetailComponent } from './components/article-detail/article-detail.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AddArticleComponent } from './components/add-article/add-article.component';
import {FormsModule} from '@angular/forms';
import { PipeTestingComponent } from './components/pipe-testing/pipe-testing.component';
import { FirstCharPipe } from './pipes/first-char.pipe';
import {ToastrModule} from 'ngx-toastr';
import { EditArticleComponent } from './components/edit-article/edit-article.component';

/*
 Nous avons ici les imports de tous nos composants. Afin dêtre utilisés, ils devront obligatoirement êtres dans
 la rubrique déclarations ci-dessous. Si nous souhaitons supprimer un composant, il faut faire attention à supprimer
 la déclaration ET SON IMPORT !
*/
@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    DashboardComponent,
    ArticleComponent,
    SportComponent,
    SanteComponent,
    FaitsDiversComponent,
    ArticleDetailComponent,
    NotFoundComponent,
    AddArticleComponent,
    PipeTestingComponent,
    FirstCharPipe,
    EditArticleComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ToastrModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
