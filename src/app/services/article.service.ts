import { Injectable } from '@angular/core';
import {Article} from '../models/article';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  typeDispo: string[] = [ 'sport', 'sante', 'faits-divers', 'politique'];
  articles: Article[];
  constructor() {
    this.articles = [
      new Article(1, 'Super match de l\'ASM', 'sport', new Date(),
        'super', 'assets/img/asm.png'),
      new Article(2, 'Mauvais match de l\'ASM', 'sport', new Date(),
        'mauvais', 'assets/img/bad.jpeg'),
      new Article(3, 'Le corona arrive', 'sante', new Date(),
        'mauvais', 'assets/img/virus.jpeg'),
    ];
  }
  getArticles(): Article [] {
    return this.articles;
  }

  getArticlesByCategory(category: string): Article[] {
    return this.articles.filter(article =>
      article.type === category);
  }

  getArticleById(id: number): Article {
    return this.articles.filter(article => article.id === id)[0];
  }

  addArticle(article: Article) {
    this.articles.push(article);
  }
  deleteArticle(articleToDelete: Article) {
    console.log(articleToDelete);
    this.articles = this.articles.filter(article =>
    article.id !== articleToDelete.id);
  }
  updateArticle(articleToUpdate: Article) {
    this.articles.filter(article => article.id === articleToUpdate.id)[0] = articleToUpdate;
  }



}
