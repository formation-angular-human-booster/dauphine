import { Component } from '@angular/core';
/*Composant de base de notre application. Il contiendra tous les composants qui doivent être affichés
Il vérifiera notre url pour afficher le bon composant*/
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'dauphine';
}
