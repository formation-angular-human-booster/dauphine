// Moule qui servire à créer nos articles. Le mot clé new Article(...)
// permettra de créer un nouvel objet en s'appuyant sur le mole ci-dessous
export class Article {
  id: number;
  titre: string;
  type: string;
  dateAjout: Date;
  contenu: string;
  image: string;
  // Fonction qui nous permettra d'initialiser un nouvel objet avec en paramètres les éléments contenus dans ().
  // Les attributs de l'objet seront ensuite initialisés en fonction des paramètres passés à notre fonction.
  // Cette fonction sera systematiquement appelée quand on utiliser le mot clé new.
  constructor(id?: number, titre?: string, type?: string, dateAjout?: Date,
              contenu?: string, image?: string) {
    this.id = id;
    this.titre = titre;
    this.type = type;
    this.dateAjout = dateAjout;
    this.contenu = contenu;
    this.image = image;
  }
}

