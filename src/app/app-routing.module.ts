import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {SanteComponent} from './components/sante/sante.component';
import {FaitsDiversComponent} from './components/faits-divers/faits-divers.component';
import {SportComponent} from './components/sport/sport.component';
import {ArticleDetailComponent} from './components/article-detail/article-detail.component';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {PipeTestingComponent} from './components/pipe-testing/pipe-testing.component';
import {AddArticleComponent} from './components/add-article/add-article.component';
import {EditArticleComponent} from './components/edit-article/edit-article.component';

/*
  Ici, nous indiquons à notre URL toutes nos routes et les composants à afficher en fonction de leur route.
  Par exemple, si j'appel l'url localhost:4200/dashboars, le composant DashboardComponent sera affiché.
  La premiere ligne fait une redirection. Si je me rends sur localhost:4200,
  je serais automatiquement redirigé sur localhost:4200/dashboars.
  La derniere ligne sert si aucun cas n'est validé. Alors, j'affiche une page de 404 disant que la requête est mauvaise.
 */
const routes: Routes = [
  {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'sante', component: SanteComponent},
  {path: 'faits-divers', component: FaitsDiversComponent},
  {path: 'sports', component: SportComponent},
  {path: 'article/add', component: AddArticleComponent},
  {path: 'article/:id', component: ArticleDetailComponent},
  {path: 'article/edit/:id', component: EditArticleComponent},
  {path: 'pipes', component: PipeTestingComponent},
  {path: '**', component: NotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
